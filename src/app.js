import Frontend from './vue/Frontend'
import moment from 'moment'
import VueCountdown from '@chenfengyuan/vue-countdown';
// require('./bootstrap');
window.Vue = require('vue');

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(value).format('HH:mm')
    }
});
Vue.component(VueCountdown.name, VueCountdown);

const frontend = new Vue({
    el: '#app',
    components: {Frontend},
});
