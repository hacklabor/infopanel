let mix = require('laravel-mix');

mix.setPublicPath('dist/.');
mix.js('src/app.js', 'dist/')
    .sass('src/app.scss', 'dist/')
    .copy("src/index.html", "dist/")
;
